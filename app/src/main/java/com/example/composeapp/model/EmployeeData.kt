package com.example.composeapp.model

data class EmployeeData(
    val empId: Int,
    val empName: String,
    val empAddress: String,
    val empPhone: String
)
