package com.example.composeapp.screen

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.example.composeapp.model.EmployeeData

@Composable
fun EmployeeListScreen() {
    Scaffold(topBar = {
        TopAppBar(
            title = { Text(text = "Employee List") },
            navigationIcon = {
                IconButton(onClick = {

                }) { Icon(Icons.Filled.ArrowBack, "backIcon") }
            },
            backgroundColor = MaterialTheme.colors.primary,
            contentColor = Color.White,
        )
    }) {
        it
        EmployeeListItem(mockData())

    }
}

@Composable
fun EmployeeListItem(mockData: List<EmployeeData>) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally, modifier = Modifier.padding(top = 10.dp)
    ) {
        Text(
            text = "Hello Employee Data List!",
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.Bold,
        )
        LazyColumn(
            Modifier.padding(top = 16.dp)
        ) {
            items(mockData) { item ->
                EmployeeItemUI(item)
            }
        }
    }

}

@Composable
fun EmployeeItemUI(employeeData: EmployeeData) {
    Card(
        shape = RoundedCornerShape(8.dp),
        backgroundColor = MaterialTheme.colors.surface,
        modifier = Modifier.padding(horizontal = 8.dp, vertical = 4.dp),
        border = BorderStroke(2.dp, Color.Red)
    ) {
        Column(modifier = Modifier.padding(10.dp)) {
            Row(
                modifier = Modifier.fillMaxHeight(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = "ID: ${employeeData.empId}",
                    color = Color.Black,
                    fontWeight = FontWeight.SemiBold,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis
                )
                Spacer(Modifier.weight(1f))
                Text(
                    text = "Name: ${employeeData.empName}",
                    color = Color.Black,
                    fontWeight = FontWeight.SemiBold,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis
                )
            }
            Text(
                text = "Mobile: ${employeeData.empPhone}",
                color = Color.Black,
                fontWeight = FontWeight.SemiBold,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis
            )
            Text(
                text = "Address: ${employeeData.empAddress}",
                color = Color.Black,
                fontWeight = FontWeight.SemiBold,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis
            )
        }

    }

}

@Composable
fun mockData(): List<EmployeeData> {
    return listOf(
        EmployeeData(1, "Amol", "Pune, Maharastra,India", "9673346489"),
        EmployeeData(2, "Monika", "Pune, Maharashtra,India", "93433346489"),
        EmployeeData(3, "Ronak", "Delhi, India", "9673346489"),
        EmployeeData(4, "Murthy", "Hyderabad, India", "9673346489"),
        EmployeeData(5, "Amol", "Pune, Maharastra,India", "9673346489"),
        EmployeeData(6, "Monika", "Pune, Maharashtra,India", "93433346489"),
        EmployeeData(7, "Ronak", "Delhi, India", "9673346489"),
        EmployeeData(8, "Murthy", "Hyderabad, India", "9673346489"),
        EmployeeData(9, "Amol", "Pune, Maharastra,India", "9673346489"),
        EmployeeData(10, "Monika", "Pune, Maharashtra,India", "93433346489"),
        EmployeeData(11, "Ronak", "Delhi, India", "9673346489"),
        EmployeeData(12, "Murthy", "Hyderabad, India", "9673346489")
    )
}
